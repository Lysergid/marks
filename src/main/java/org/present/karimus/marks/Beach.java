package org.present.karimus.marks;

import java.util.Arrays;

public class Beach {
    public static void main(String[] args) {
        Dog jack = new Dog(52, 20, 6);
        Dog hodor = new Dog(52, 20, 13);
        Dog adolf = new Dog(52, 20, 34);
        Dog hitler = new Dog(52, 20, 73);
        Dog[] dogs = new Dog[]{adolf, hodor, hitler, jack};
        System.out.println(Arrays.toString(dogs));
        for (int i = 0; i < dogs.length; i++) {
            for (int j = 0; j < dogs.length - 1; j++) {
                int diff = dogs[j].compareTo(dogs[j+1]);
                System.out.println(i+"//"+j+"//"+diff);
                if (diff > 0) {
                    Dog buffer = dogs[j];
                    dogs[j] = dogs[j+1];
                    dogs[j] = buffer;
                    System.out.println(Arrays.toString(dogs));
                }
            }
        }
        System.out.println(Arrays.toString(dogs));
    }
}
