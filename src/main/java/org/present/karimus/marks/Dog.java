package org.present.karimus.marks;

public class Dog implements Comparable<Dog> {
    private int height;
    private int weight;
    private int age;

    Dog(int height, int weight, int age) {
        this.height = height;
        this.weight = weight;
        this.age = age;
    }

    public String toString() {
        return "{ Height = " + height + ", weight = " + weight + ", age = " + age + "}";
    }

    public void feed() {
        weight++;
    }

    public int getAge(){
        return this.age;
    }

    public boolean equals(Dog dog) {
        return dog != null && height == dog.height && weight == dog.weight && age == dog.age;
    }

    public int compareTo(Dog dog) {
        if(dog == null){
            return 1;
        }
        if(this.age == dog.age){
            return 0;
        }
        if (this.age > dog.age) {
            return 1;
        }
        return -1;
    }
}

